# MetaDataForm Overview

This is a concept, process and tools based around a meta-language for defining data based interfacing.

The idea is that an interface designed in one base technology can be exported to a text file (XML probably) as a definition. That definition can then be interpreted by a different platform and rendered as something functionally equivalent.

It will start out with a modest set of features, and hopefully not lock in any unecessary limitations.

It has been prompted by desire for a solution to the problem of "porting" data based applications such as those made in Microsoft Access. As some people will have a fair number of these - and because the scale of these can be quite large, it would be useful to have tools and process to do as much as possible.

There does already exist some VBA code for dumping the data of an Access system ready for importing to a Postgres database, but this only covers the data (and to a lesser extent, the queries).

Indeed, from a functioning Access application there would be three things to convert:
- the data/database;
- the program code that does various automated actions;
- the GUI forms for editing the data and triggering the actions.

Thus there is a solution for the first. It is likely that there is no feasible solution for the second (long story but basically about
programming language conversion not being a realistic thing).

Therefore this project is about tackling the third thing. All the GUI form designs. With reference to Access, the forms are really more a kind of "dataform" as most have an associated "recordset" (a table or query) that they inherently know how to navigate. While there are many "frameworks" and libraries around (for web and native applications respectively) very few provide readymade "dataform" support. 

To tackle it, devised here is a multi-part strategy that requires simultaneous development of complementary pieces.
- for Access, VBA code to read all the form definitions and write them out in a specific neutral form - as an XML file;
- for the target environment (or for each intended target) something to read the XML file and build an equivalent GUI system.

While that looks like two steps, it's really using yet two more:
- the XML requirements need to be worked out as something suitable;
- a way of reading the _custom_ XML needs to be written as a stage - e.g. to an in-memory object model - to then be used for enacting the various targets.

The initial implementation is to write the XML interpretation, object model and target generators in Python. At time of writing, what has been achieved is a proof-of-concept of combining:
- a good-enough staring point for the XML design;
- a Python program that reads the XML and generates interactive GUI forms with the "Tkinter" Python library.

It's expected to not be too hard to write the VBA code for creating the XML from an Access applications Forms, so that will deferred to when the XML design is stable and mostly feature complete. 

It is intended to also write - in Python - a generator for some kind of Web serving interface for having an application delivered to a web browser. An example would be to do that with Py4Web - but that might also wait until somewhat later.

## Development Sequence

- Design metaform system
- Implement metaform for Tkinter - in Python
- Implement Access forms to metaform - in VBA - see [MetaDataFormVbaAccess](https://gitlab.com/geraldew/metadataformvbaaccess)
- Implement metaform for Py4Web - in Python

# MetaDataForm Tkinter Annotation

This is annotation for the initial trial implementation of the MetaDataForm definition into Python + Tkinter

# Current XML

See the example file in this repository for a literal example.

Here are the tag names and their intended meanings.

- mdfml = a wrapper tag for the MetaDataForm content in an XML file.
- defquad = a definition of a data-interaction-form. This is needed as an intermediary to emulate how in MS Access, a "Form" might be used as a window or as a subform. For no good reason, the intermediary is a "quad". There'll be another tag for quoting its use.
- one_uid = used inside another tag to give the item a unique value for referencing elsewhere.
- datatray = another intermediary inside a data-interaction-form - this time to allow the quad to have some elements/widgets outside the "data" section.
- dataform = this is one-record-at-a-time data form
- one_name = a way to specify a name
- ctrl_label = a Label widget - i.e. just a display of short text
- one_caption = the text displayed in a Lable widget
- dataref = this will be for directing the database query/table for a datatray to handle - details of this yet to be determined.
- subdiv = this is an organising element, mainly for group control of layouts for widgets. At the moment the idea is for this to translate into a Tkinter "Frame" but may yet evolve. 
- ctrl_textbox = an input & display widget for short text
- ctrl_listbox = a listbox widget
- ctrl_combobox = a combobox widget - i.e. a pull-down list variant of a list box
- ctrl_checkbox = a checkbox widget - worth noting here that the "radio-button" style is not intended to be supported as its purpose can be emulated by a combox or listbox
- ctrl_button = a button widget - with the intention that this will have some way of referring to action code in the target progamming language environment - details yet to be determined
- window = a form-as-window - this will be equivalent to an Access form when used directly (not as a subform). As this is only a directive to make a window level interaction, it will require the following tag to actually have some content. 
- usequad = a quotation of the id of a "quad" to be used. This will later be given means to specify the data connection between the host of the usequad and the quad being used. The two places where usequad will appear is either: inside a window tag, or inside a defquad to enact usage as a subform. 

# Code structure

At time of writing, 

- the current process is a two-stage affair
- stage 1 is reading the XML and making a tree of interim-custom objects
- stage 2 is traversing the custom objects and build Tkinter objects

To do that, there are two enumerations defined:
- MdfmlTag = where each value is for a specific XML tag
- Obj4GuiTyp = where each value is for a type of interim-custom object or anything else that needs handling

Associated with the Obj4GuiTyp values are a series of class definitions. Currently these are mainly trivial and the same. They are likely to evolve along their own paths as development gets into the details.

There may also yet become more middle-classes - currently there are two:
- Generic - mainly for the simpler widgets
- Container - for things that can contain other things

Note that when the stage 2 traversal occurs, a split happens between the "complex" object types and the rest. This isn't about which are containers or not, rather is there to allow the idea of something that has a single definition in the interim object layer but will be enacted by a combination of controls in the target (currently just Tkinter).

If it's not clear from anywhere else, the idea is not to write the "interim-object to target" code in a way as to handle multiple targets. Rather the intention is to write separate code for each target. In the long run that might mean a module for each.

Another dummy reference in the code is `gui_GeneratePythonCode` which is just a mental note to consider writing a Python code generator for Tkinter as an option. Such that a whole set of "data form" Tkiner code would then be a *static* code base going forward (rather than the live generation of Tkinter objects). Frankly, this is the kind of thing most other framework conversions do - and which seems inadequate for large and/or ongoing project conversions - so while an obvious thing to do sometime, is a low priority in the project. 

## Test application

As an application the current code does one of two things, depending on the True/False usage in the main block (yes, this is all very rudimentary at this point).

```Python
	if True :
		mdf_Tkinter_LiveMakeSelection( )
	elif True :
		Load_XML_and_Build_GUI_Test()
```

where:
-  `Load_XML_and_Build_GUI_Test` does a fixed run of reading a specific XML file and enacting the first "window" in it as a single Tkinter window.
- `mdf_Tkinter_LiveMakeSelection` opens an intractive window where a user can select an XML file, have it loaded into a interim-custom object set, select one defined "window", then "Open" that as a new child window to see how it looks.

## Flotsam and Jetsom

Yes, there's a few other things sitting around in the code waiting to be used. Such as a block of code for handling a SQLite database. Similarly there's some file selection

# Next Steps

At the moment, the proof-of-concept has been reached. From here there are several paths forward:
- get more details of control settings and layout enacted
- start building the ability to connect to data in a datatray - e.g. using a Tkinter Treevew as a datasheet
- extend the cross-referencing to enact the parent-child linking of two DataTray items.

All of those will require a simultaneous addition of an XML tag, its handling, an interim-object for it, and an enactment into Tkinter objects. Luckly the Tkinter aspects have already been proven in other Python Tkinter examples:
- [Python Example Data Form Parent Children Minimal](https://gitlab.com/geraldew/example-dataform-parentchildren-minimal)
- [DataFormTrials-ParentChildren](https://gitlab.com/geraldew/dataformtrials-parentchildren)

# Other Future Intentions

## DataTray navigation 

Yet to be implemented is the stock navigation controls for data records inside a DataTray.

Currently intended ideas are buttons for:
- Toggling Between SingleRecord and TabularMultiRecords - i.e. between the DataForm and DataSheet objects

Record navigation buttons:
- Goto First Record
- Goto Backward X Records
- Goto Previous Record
- Goto Next Record
- Goto Foreward X Records
- Goto Last Record
- enable an Edit mode
- Save the record
- Clear the record
- Reload - discard the edit
- Make a new record

some nice to have Per-Control actions:
- Clear
- Copy in from previous record
- Increment value
- Decrement value
- Refresh/Reload - e.g. for ListBox and ComboBox and SubForm


## Software License

This is Free Open Source Software (FOSS) using the GPL3 license.

As a note to those who may find that concerning, let me be clear:
- the author has followed the evolution of FOSS since its birth and is well versed in the concepts;
- Yes, I do know that GPL is not the norm for Python applications;
- Copyright - and thus the license - applies to my specific code, so a fork of this source code has to also respect and apply the GPL;
- FOSS means you are free to study the code and use what you learn to write your own software, where you can thereby choose a license. No need to ask, please go ahead and do that.

# Screenshots

![Demonstrator window](doco/screenshots/MetaDataForm_Demonstrator.png)

![Example generated window 1](doco/screenshots/MetaDataForm_Example_quad_1.png)

![Example generated window 2](doco/screenshots/MetaDataForm_Example_quad_2.png)

## Northwind Examples

The list of forms seen from Loading the XML generated by the AccessVBA module:

![Example generated window 2](doco/screenshots/Northwind_ListOfForms.png)

A selection of Tkinter windows generated from the XML being converted to Tkinter objects:

![Example generated window 2](doco/screenshots/Northwind_Form_Employees.png)
![Example generated window 2](doco/screenshots/Northwind_Form_Orders.png)
![Example generated window 2](doco/screenshots/Northwind_Form_Products.png)
![Example generated window 2](doco/screenshots/Northwind_Form_QuarterlyOrders.png)
![Example generated window 2](doco/screenshots/Northwind_Form_QuarterlyOrdersSubform.png)
![Example generated window 2](doco/screenshots/Northwind_Form_SalesReportsDialog.png)
![Example generated window 2](doco/screenshots/Northwind_Form_Suppliers.png)

For a more controlled test subject, I made a small Access database, that manages a simple many-to-may structure using three tables:
- This - each row with a unique ID
- That - each row with a unique ID
- ThisWithThat - which has entries pointing to the other two tables so as to store combinations of them

As part of that is a simple structure of Forms and Subforms - so as to view one list, and see/edit the links to the other.

![This items with subform of That](doco/screenshots/ThisAndThat_Access_FormThisAndWithThat.png)
![Thas items with subform of This](doco/screenshots/ThisAndThat_Access_FormThatAndWithThis.png)
![the two subforms on their own](doco/screenshots/ThisAndThat_Access_Subforms.png)

And here is an example of one of the subforms converted via this tooling. Note that no manual editing is being done here. The Access module converts to MDFML (MetaDataForm XML) and then this tool reads that and composes equivalent structures with Tkinter.

![Tkinter version of one of the subforms](doco/screenshots/ThisAndThat_Tkinter_SubformThisThat_That.png)

Note also, the recent added feature by which, the DataTray concept is rendered with:
- a frame on the left side to hold record navigation buttons
- next to that is a sideways tabbed Notebook with two standard items for DataForm and DataSheet
(of course, currently there is no action behind the buttons and no data is being handled yet)

This is my proposed replacement method for the stock navigation controls that access put at the bottom of each Form/Subform.

Addendum: am now able to populate the datasheet in a datatray - by referencing a table/view specified in the MDFML file.

![Populated datasheet in a datatray](doco/screenshots/ThisAndThat_Tkinter_SubformThisThat_That_DataSheet.png)

Note: the formatting isn't working properly yet, hence the scroll bars are in the wrong place.
