#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Python Example metadatformtkinter
#
# --------------------------------------------------
# Copyright (C) 2021-2021  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import sys as im_sys
#import os as im_os
#import os.path as osp


import tkinter as im_tkntr 
import tkinter.ttk as im_tkntr_ttk
#import tkinter.scrolledtext as im_tkntr_tkst
from tkinter import scrolledtext as im_tkntr_tkst 
from tkinter import filedialog as im_filedialog 
from tkinter import messagebox as im_messagebox

import pathlib as im_pathlib

import sqlite3 as im_sql
from sqlite3 import Error as fi_Error

import xml.etree.ElementTree as im_ET

# from collections import namedtuple

from enum import Enum, unique, auto

import tkinter.font as m_tkntr_font 

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

# --------------------------------------------------
# set up custom named tuples

# tuple for items in the contains lists of Tk objects
# tpl_TkObj = namedtuple('tpl_TkObj', 'typ obj')

# --------------------------------------------------
# Global scope objects
# --------------------------------------------------

# This needs to be here, not for time priority but for global scope
# global thismodule_log 

# --------------------------------------------------
# Debug Temp
# --------------------------------------------------

def debug_filename() :
	return "/home/ger/mdf_tk_debug_trace.txt"

def dclear() :
	with open( debug_filename(), "w") as logfile:
		logfile.write( debug_filename() + "\n")
		print( "Logging to: " + debug_filename() )

def dprint( p_s ):
	with open( debug_filename(), "a") as logfile:
		logfile.write( p_s + "\n")
		print( p_s )

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def app_code_name():
	return "metadataform_tk"

def app_code_abbr():
	return "mdft"

def app_show_name():
	return "Python MetaDataForm Example Implementation"

def app_code_abbr_s():
	return app_code_abbr() + " "

def app_version_code_show() :
	return "v0.0.2"
def app_show_name_version() :
	return app_show_name() + " " + app_version_code_show()

def app_show_subtitle():
	return "Development Test Bed"


# --------------------------------------------------
# Example File references
# --------------------------------------------------


def ExampleDetail_Database_Filename():
	return "../metadataform_example_dbase.db"

def ExampleDetail_MetaDataForm_Filename():
	return "../metadataform_example_mdf.xml"

# --------------------------------------------------
# Support Stock
# --------------------------------------------------

# --------------------------------------------------
# Counter object - making up for Python not having byref scalar parameters  

class IdCounter( ):
	def __init__(self):
		self.id = 0
	def Incr(self):
		self.id += 1
		# dprint( "IdCounter: " + str(self.id))
	def Get(self):
		return self.id

# --------------------------------------------------
# == SQLite Wrappers

def sqlite_close_connection(conn):
	didok = False
	try:
		if conn:
			conn.close()
			didok = True
	except fi_Error as e:
		dprint("SQLite Error: " + str(e) )
	return didok

def sqlite_create_connection_to_file(db_file):
	""" create a database connection to a SQLite database """
	conn = None
	didok = False
	try:
		conn = im_sql.connect(db_file)
		#dprint("SQLite Version: " + sqlite3.version + " " + db_file)
		didok = True
	except fi_Error as e:
		dprint("SQLite Error: " + str(e) )
	return didok, conn

def sqlite_create_connection_in_memory():
	""" create a database connection to a database that resides
		in the memory
	"""
	conn = None;
	didok = False
	try:
		conn = im_sql.connect(':memory:')
		#dprint("SQLite Version: " + im_sql.version)
		didok = True
	except fi_Error as e:
		dprint("SQLite Error: " + str(e) )
	return didok, conn

def sqlite_create_connections_main_in_memory_output_as_file( outdb_filename, outdb_refname ):
	""" create a database connection to a database that resides
		in the memory
	"""
	conn = None;
	didok = False
	try:
		conn = im_sql.connect(':memory:')
		#dprint("SQLite Version: " + im_sql.version)
		didok = True
	except fi_Error as e:
		dprint("SQLite Error: " + str(e) )
	if didok :
		didok = False
		try:
			conn.execute('ATTACH DATABASE ' + outdb_filename + ' AS ' + outdb_refname)
			didok = True
		except fi_Error as e:
			dprint("SQLite Error: " + str(e) )
	return didok, conn

def sqlite_establish_connection( p_do_file, p_pathfilename ):
	# p_do_file == boolean 
	# p_pathfilename == 
	# this just allows dev-time switching between methods 
	if p_do_file and len( p_pathfilename ) > 0 :
		didok, conn = sqlite_create_connection_to_file( p_pathfilename )
		if didok :
			dprint( "Connected to " + p_pathfilename )
	else :
		didok, conn = sqlite_create_connection_in_memory()
		if didok :
			dprint( "Connected to memory database" )
	return didok, conn
	# didok, conn = sqlite_establish_connection()

def sqlite_execute_sql_get_lastrowid( conn, str_sql):
	""" create a table from the create_table_sql statement
	:param conn: Connection object
	:param create_table_sql: a CREATE TABLE statement
	:return:
	"""
	didok = False
	got_lastrowid = 0
	try:
		c = conn.cursor()
		c.execute( str_sql )
		didok = True
		got_lastrowid = c.lastrowid
		conn.commit()
	except fi_Error as e:
		dprint("SQLite Error: " + str(e) )
	return didok, got_lastrowid

def sqlite_execute_sql( conn, str_sql):
	didok, got_lastrowid = sqlite_execute_sql_get_lastrowid( conn, str_sql)
	return didok

def sqlite_fetchwith_sql( conn, str_sql):
	didok = False
	try:
		c = conn.cursor()
		c.execute( str_sql )
		rows = c.fetchall()
		didok = True
	except fi_Error as e:
		dprint("SQLite Error: " + str(e) )
		rows = []
	return didok, rows

def sqlite_fetchwith_sql_print_n( conn, str_sql, n_rows):
	didok = False
	try:
		c = conn.cursor()
		c.execute( str_sql )
		rows = c.fetchall()
		didok = True
	except fi_Error as e:
		dprint("SQLite Error: " + str(e) )
		rows = []
	if didok :
		# print row of column names
		names = list( map( lambda x: x[0], c.description) )
		# dprint( "\t".join( names ) )
		# print rows of data
		if False:
			# pending adding data type handling
			n = 1
			while n < n_rows and n <= len(rows) :
				dprint( "\t".join( rows[n - 1] ) )
	return didok, rows

def sqlite_fetchwith_sql_with_column_names( conn, str_sql):
	didok = False
	try:
		c = conn.cursor()
		c.execute( str_sql )
		rows = c.fetchall()
		didok = True
	except fi_Error as e:
		dprint("SQLite Error: " + str(e) )
		rows = []
		names = []
	if didok :
		names = list( map( lambda x: x[0], c.description) )
	return didok, rows, names


# --------------

def SqLite_Filename_GetTableList( p_fn):
	i_lst = []
	didok, conn = sqlite_create_connection_to_file( p_fn)
	if didok :
		str_sql = "SELECT name FROM sqlite_master WHERE type='table';"
		didok, rows = sqlite_fetchwith_sql( conn, str_sql)
		if didok :
			for row in rows:
				i_lst.append( row[0])
	else:
		i_lst = []
	return i_lst

def SqLite_Tablename_GetColumnList( p_filename, p_tablename):
	i_lst = []
	didok, conn = sqlite_create_connection_to_file( p_filename)
	if didok :
		str_sql = "pragma table_info('" + p_tablename + "');"
		didok, rows = sqlite_fetchwith_sql( conn, str_sql)
		if didok :
			for row in rows:
				i_lst.append( row[1])
		else:
			i_lst = []
	else:
		i_lst = []
	return i_lst

def SqLite_Filename_GetTable_Rows( p_fn, p_tblnm ):
	didok = False
	rows = []
	names = []
	didok, conn = sqlite_create_connection_to_file( p_fn)
	if didok :
		str_sql = "SELECT * FROM " + p_tblnm + " ;"
		didok, rows, names = sqlite_fetchwith_sql_with_column_names( conn, str_sql)
	return didok, rows, names

def SqLite_Filename_GetTable_Rows_Where_ForeignKey_IsValue( p_fn, p_tblnm, p_fkc, p_kval ):
	didok = False
	rows = []
	names = []
	didok, conn = sqlite_create_connection_to_file( p_fn)
	if didok :
		str_sql = "SELECT * FROM " + p_tblnm + " WHERE " + p_fkc + " = " + str(p_kval) + " ;"
		didok, rows, names = sqlite_fetchwith_sql_with_column_names( conn, str_sql)
	return didok, rows, names


# --------------------------------------------------
# Class extentions - tkinter
# --------------------------------------------------

# --------------------------------------------------
# Custom Settings Supports
# --------------------------------------------------

# --------------------------------------------------
# Enumeration for Tag values
# --------------------------------------------------

@unique
class MdfmlTag(Enum):
	NoTag = "NoTag" # not really used
	#
	mdfml = "mdfml" # this is required for any valid MetaDataForm Markup Language file
	# interface structures
	window = "window" # indepedent display window, usually employing a quad
	defquad = "defquad" # a named container - used for populating a window or a subform
	usequad = "usequad" # window attribute to point to a quad
	# the data tray( form,sheet) 
	datatray = "datatray" # a holder of a dataform and/or datasheet
	dataform = "dataform" #  a form or subform, an interactive
	datasheet = "datasheet" # grid of a dataset fetched via a dataref
	# unique identifier
	one_uid = "one_uid" #  identifying name, must be unique in its context
	# data attributes
	dataref = "dataref" #  a data reference
	rstref = "rstref" # the name of a recordset (table or query)
	# usedataref = "usedataref" #  definition of a data query reference, a named element
	#
	one_name = "one_name" #  identifying name, must be unique in its context
	form_mode = "form_mode" #  form|sheet
	#one_default_dataref = "one_default_dataref" #  which dataref name is the default/initial one
	#one_default_form_mode = "one_default_form_mode" #  which form_mode is the default/initial one
	# the simple controls
	ctrl_label = "ctrl_label" #  a label control
	ctrl_textbox = "ctrl_textbox" #  a textbox control
	ctrl_listbox = "ctrl_listbox" #  a listbox control
	ctrl_combobox = "ctrl_combobox" #  a combobox control
	ctrl_checkbox = "ctrl_checkbox" #  a checkbox control
	ctrl_button = "ctrl_button" #  a button control
	# tab/page controls
	ctrl_tabset = "ctrl_tabset" # 
	ctrl_subtab = "ctrl_subtab" # 
	# control attributes
	one_caption = "one_caption" #   a display text for something
	# layout directives
	subdiv = "subdiv" #  subdivision, frame
	#one_ctrl = "one_ctrl" #  for a label, the name of its associated control
	#one_datacol = "one_datacol" #  the data column supplying/supplied-by the control
	#one_datacol_name = "one_datacol_name" #  the data column providing the controls selected value 
	#see_datacol_name = "see_datacol_name" #  a data column to be displayed
	#one_click_actiontype = "one_click_actiontype" #  values:(form_action_exit|etc)
	#one_click_actionref = "one_click_actionref" #  reference to code element
	#one_uform_datalink = "one_uform_datalink" #  definitinon of subform-to-form data relation
	#link_pair = "link_pair" #  two lists of joining columns
	#one_busform_datacol = "one_busform_datacol" #  list of joining colums in the busform
	#one_subform_datacol = "one_subform_datacol" #  list of joining colums in the subform
	place_x = "place_x"
	place_y = "place_y"
	place_w = "place_w"
	place_h = "place_h"

def MdfTag_GetList() :
	return list(map(lambda c: c.value, MdfmlTag))

def MdfTag_GetTuple() :
	return tuple( MdfTag_GetList() )

def MdfTag_Default() :
	return MdfmlTag.NoTag

def MdfTag_EnumOfString( s ) :
	#dprint( "MdfTag_EnumOfString")
	#dprint( s)
	m = MdfTag_Default()
	for member in MdfmlTag :
		#dprint( "test " + member.value)
		if member.value == s :
			m = member
			#dprint( "Bingo!")
			#dprint( m)
			break
	return m

def MdfTag_EnumOfName( s ) :
	# Usage: eok, enm = MdfTag_EnumOfName( s ) 
	eok = False
	enm = MdfmlTag.NoTag
	for member in MdfmlTag :
		if member.name == s :
			enm = member
			eok = True
			break
	return eok, enm

def MdfTag_EnumValueString( e ) :
	try:
		s = e.value
	except:
		s = ""
	return s

def MdfTag_IsGuiObj() :
	return [ \
		MdfmlTag.window, MdfmlTag.defquad, MdfmlTag.usequad, \
		MdfmlTag.datatray, MdfmlTag.dataform, MdfmlTag.datasheet, \
		MdfmlTag.one_uid, MdfmlTag.one_name, MdfmlTag.form_mode, \
		MdfmlTag.ctrl_label, MdfmlTag.ctrl_textbox, MdfmlTag.ctrl_listbox, MdfmlTag.ctrl_combobox, \
		MdfmlTag.ctrl_checkbox, MdfmlTag.ctrl_button, \
		MdfmlTag.ctrl_subtab, MdfmlTag.ctrl_tabset, \
		MdfmlTag.one_caption, MdfmlTag.subdiv, 
		MdfmlTag.place_x, MdfmlTag.place_y, MdfmlTag.place_w, MdfmlTag.place_h ]

def MdfTag_IsDataRef() :
	return [MdfmlTag.dataref, MdfmlTag.rstref]

def MdfTag_TagStr_IsGuiObj( s ) :
	return s in list(map(lambda c: c.value, MdfTag_IsGuiObj() ) )

def MdfTag_TagStr_IsDataRef( s ) :
	return s in list(map(lambda c: c.value, MdfTag_IsDataRef()))

# --------------------------------------------------
# Holding structures for middle layer data definitions
# --------------------------------------------------

# --------------------------------------------------
# Interim - objects just for use during the XML decoding

@unique
class Obj4DataTyp(Enum):
	#O4D_Generic = auto() # not really used, just a placeholder
	#O4D_Root = auto() # a dummy type
	O4D_DataRef = auto() #
	#O4D_RstRef = auto() # reserved for when allowing momre than just table/query names to be quoted

class Xml2Obj4Data( ):
	def __init__( self, p_typ ):
		self.typ = p_typ
		self.prop = {}
	def add_property( self, p_key, p_str ):
		self.prop[ p_key ] = p_str

# --------------------------------------------------
# Main container

@unique
class Obj4Data_DataBaseTyp(Enum):
	Sqlite = auto() # default and initial implementation of "the database" being a single SQLite file

# Structure of an Object for each query reference. 

class Obj4Data_DataRef( ):
	def __init__(self, idnum ):
		self.id = idnum
	#def set_Title( self, p_str ):
	#	self.title = p_str
	def set_rstref( self, p_str ):
		self.rstref = p_str
	def get_rstref( self ):
		return self.rstref

# Structure of an Object to hold the database reference and a dictionary of all the query references

class Obj4Data_DataBase( ):
	def __init__(self, p_dbtype ):
		self.dbtyp = p_dbtype
		self.db_filename = ""
		self.dct_DataRefs = {}
	def set_db_filename( self, p_filename ):
		self.db_filename = p_filename
	def get_db_filename( self ):
		return self.db_filename
	def set_DataRef( self, p_DataRef ):
		self.dct_DataRefs[ p_DataRef.id ] = p_DataRef
	def get_DataRef( self, p_idnum ):
		return self.dct_DataRefs[ p_idnum ]

# --------------------------------------------------
# Holding structure for middle layer object definitions
# --------------------------------------------------

@unique
class Obj4GuiTyp(Enum):
	#Generic = auto() # not really used, just a placeholder
	# order doesn't matter, but doing these alphabetic for easy checking
	# as this is about Tkinter elements, to be names matching Tkinter gadget concepts 
	Root = auto() # a dummy type, used for the holders of Windows 
	Window = auto() #
	DefQuad = auto() #
	UseQuad = auto() #
	DataTray = auto() #
	DataForm = auto() #
	DataSheet = auto() #
	# Native Controls
	Label = auto() #
	Button = auto() #
	TextBox = auto() # aka Textbox
	CheckBox = auto() #
	ListBox = auto() #
	ComboBox = auto() #
	#? LabelFrame = auto() #
	# The Tabs
	TabSet = auto() #
	SubTab = auto() #
	# MultiLine Text
	ScrolledText = auto() #
	# Layout indicator
	Frame = auto() #
	# Meta Constructs - pseudo-objects that are automated combinations of other controls

def Obj4GuiTyp_Str( p_Obj4GuiTyp ) :
	# just for use during debugging, not intended to be needed
	if p_Obj4GuiTyp == Obj4GuiTyp.Root :
		return "Root"
	elif p_Obj4GuiTyp == Obj4GuiTyp.Window :
		return "Window"
	elif p_Obj4GuiTyp == Obj4GuiTyp.DefQuad :
		return "DefQuad"
	elif p_Obj4GuiTyp == Obj4GuiTyp.UseQuad :
		return "UseQuad"
	elif p_Obj4GuiTyp == Obj4GuiTyp.DataTray :
		return "DataTray"
	elif p_Obj4GuiTyp == Obj4GuiTyp.DataForm :
		return "DataForm"
	elif p_Obj4GuiTyp == Obj4GuiTyp.DataSheet :
		return "DataSheet"
	elif p_Obj4GuiTyp == Obj4GuiTyp.Label :
		return "Label"
	elif p_Obj4GuiTyp == Obj4GuiTyp.Button :
		return "Button"
	elif p_Obj4GuiTyp == Obj4GuiTyp.TextBox :
		return "TextBox"
	elif p_Obj4GuiTyp == Obj4GuiTyp.CheckBox :
		return "CheckBox"
	elif p_Obj4GuiTyp == Obj4GuiTyp.ListBox :
		return "ListBox"
	elif p_Obj4GuiTyp == Obj4GuiTyp.ComboBox :
		return "ComboBox"
	elif p_Obj4GuiTyp == Obj4GuiTyp.TabSet :
		return "TabSet"
	elif p_Obj4GuiTyp == Obj4GuiTyp.SubTab :
		return "SubTab"
	elif p_Obj4GuiTyp == Obj4GuiTyp.ScrolledText :
		return "ScrolledText"
	elif p_Obj4GuiTyp == Obj4GuiTyp.Frame :
		return "Frame"
	else :
		return "Uknown"

# --- First some underlying classes

# Base class - i.e. not for direct use
class Obj4Gui__Generic( ):
	# name = ""
	def __init__(self, idnum):
		#self.isTyp = Obj4GuiTyp.Generic
		self.id = idnum
		self.ident = "ObjId_" + str(idnum) # temp/dummy value, should get replaced by a one_name tag
		self.name = ""

# Derived Base class - i.e. not for direct use 
class Obj4Gui__Generic_Placable( Obj4Gui__Generic ):
	def __init__(self, idnum):
		#self.isTyp = Obj4GuiTyp.Generic
		self.id = idnum
		self.place_x = 0
		self.place_y = 0
		self.place_w = 0
		self.place_h = 0
	def set_place_x( self, p_x ):
		self.place_x = int(p_x)
	def set_place_y( self, p_y ):
		self.place_y = int(p_y)
	def set_place_w( self, p_w ):
		self.place_w = int(p_w)
	def set_place_h( self, p_h ):
		self.place_h = int(p_h)
	def unplaced( self ) :
		return self.place_x + self.place_y + self.place_w + self.place_h == 0

# Derived Base class - i.e. not for direct use 
class Obj4Gui__Container( Obj4Gui__Generic ):
	def __init__(self, idnum):
		#self.isTyp = Obj4GuiTyp.Generic
		self.contains = []
		self.id = idnum
	def add_SubObj( self, p_obj ):
		# i_SubObj = tpl_TkObj( p_typ, p_obj )
		self.contains.append( p_obj )

class Obj4Gui__Container_Placable( Obj4Gui__Container ):
	def __init__(self, idnum):
		#self.isTyp = Obj4GuiTyp.Generic
		self.contains = []
		self.id = idnum
		self.place_x = 0
		self.place_y = 0
		self.place_w = 0
		self.place_h = 0
	def set_place_x( self, p_x ):
		self.place_x = int(p_x)
	def set_place_y( self, p_y ):
		self.place_y = int(p_y)
	def set_place_w( self, p_w ):
		self.place_w = int(p_w)
	def set_place_h( self, p_h ):
		self.place_h = int(p_h)
	def unplaced( self ) :
		return self.place_x + self.place_y + self.place_w + self.place_h == 0


# --- Now the various specific classes, one for object type
# this is to allow each to evolve independently

# the conceptual organising objects

class Obj4Gui_Root( Obj4Gui__Container ):
	isTyp = Obj4GuiTyp.Root

class Obj4Gui_Window( Obj4Gui__Container ):
	isTyp = Obj4GuiTyp.Window

class Obj4Gui_DefQuad( Obj4Gui__Container ):
	isTyp = Obj4GuiTyp.DefQuad

class Obj4Gui_UseQuad( Obj4Gui__Container ):
	isTyp = Obj4GuiTyp.UseQuad

class Obj4Gui_DataTray( Obj4Gui__Container ):
	isTyp = Obj4GuiTyp.DataTray
	hasFrm = False
	hasSht = False
	Obj4Gui4Frm = None
	Obj4Gui4Sht = None
	# data obj
	hasRst = False
	Obj4Dta4Rst = None
	# Gui methods
	def Set_Obj_4Frm( self, p_Obj ):
		self.Obj4Gui4Frm = p_Obj
		self.hasFrm = True
	def Set_Obj_4Sht( self, p_Obj ):
		self.Obj4Gui4Sht = p_Obj
		self.hasSht = True
	def Has_Obj_4Frm( self ):
		return self.hasFrm
	def Has_Obj_4Sht( self ):
		return self.hasSht
	def Get_Obj_4Frm( self ):
		if self.hasFrm :
			return self.Obj4Gui4Frm
		else :
			return None
	def Get_Obj_4Sht( self ):
		if self.hasSht :
			return self.Obj4Gui4Sht
		else :
			return None
	# Data methods
	def Set_DataRef_id( self, p_idnum ):
		self.DataRef_id = p_idnum
		self.hasRst = True
	def Has_DataRef_id( self ):
		return self.hasRst
	def Get_DataRef_id( self ):
		if self.hasRst :
			return self.DataRef_id
		else :
			return None

class Obj4Gui_DataForm( Obj4Gui__Container ):
	isTyp = Obj4GuiTyp.DataForm

class Obj4Gui_DataSheet( Obj4Gui__Generic ):
	isTyp = Obj4GuiTyp.DataSheet

# the ones for Controls/Widgets

class Obj4Gui_Button( Obj4Gui__Generic_Placable ):
	isTyp = Obj4GuiTyp.Button
	Button_text = ""

class Obj4Gui_Checkbutton( Obj4Gui__Generic_Placable ):
	isTyp = Obj4GuiTyp.CheckBox
	Checkbutton_text = ""
	def textvariable_set():
		pass

class Obj4Gui_Combobox( Obj4Gui__Generic_Placable ):
	isTyp = Obj4GuiTyp.ComboBox
	def textvariable_get():
		pass

class Obj4Gui_TextBox( Obj4Gui__Generic_Placable ):
	isTyp = Obj4GuiTyp.TextBox
	def textvariable_get():
		pass

class Obj4Gui_Frame( Obj4Gui__Container_Placable ):
	isTyp = Obj4GuiTyp.Frame
	# Frame_fill
	# Frame_bg
	pass

class Obj4Gui_Label( Obj4Gui__Generic_Placable ):
	isTyp = Obj4GuiTyp.Label
	Label_text = ""

#class Obj4Gui_LabelFrame( Obj4Gui__Generic ):
#	isTyp = Obj4GuiTyp.LabelFrame
#	Frame_text = ""

class Obj4Gui_Listbox( Obj4Gui__Generic_Placable ):
	isTyp = Obj4GuiTyp.ListBox
	Frame_text = ""

class Obj4Gui_ScrolledText( Obj4Gui__Generic ):
	isTyp = Obj4GuiTyp.ScrolledText
	def textvariable_get():
		pass

class Obj4Gui_SubTab( Obj4Gui__Container ):
	isTyp = Obj4GuiTyp.SubTab
	pass

class Obj4Gui_TabSet( Obj4Gui__Container_Placable ):
	isTyp = Obj4GuiTyp.TabSet
	pass

# Now we have the Meta-Object, this acts as an extra featured container for the hierarchial structure of Obj4Gui
# In addition, it has dictionaries alllowing cross linking between named objects
# This is required to provide a way for Windows to know which DefQuad objects they UseQuad
# Unlike the other Obj4Gui objects, these don't have an id nor an IsTyp as there's only expected to be one of these in existence
# per XML file read
   
class Obj4Gui_Meta( Obj4Gui__Container ):
	def __init__(self):
		self.contains = []
		self.DctWindows = {}
		self.DctDefQuads = {}
	def lnk_SubObj( self, p_obj ):
		self.contains.append( p_obj )
	def DctWindows_lnk_Obj( self, p_obj, p_id ):
		self.DctWindows[ p_id ] = p_obj
	def DctDefQuads_lnk_Obj_Ident( self, p_obj, p_ident ):
		if p_ident in self.DctDefQuads :
			dprint( "Ooops, already have a DefQuad of that name! " + p_ident )
		else :
			dprint( "Cool, just linked a DefQuad of name: " + p_ident )
		self.DctDefQuads[ p_ident ] = p_obj
	def get_lst_DefQuad_Idents( self ):
		return list( self.DctDefQuads.keys)
	def get_DefQuad_by_Ident( self, p_ident ):
		if p_ident in self.DctDefQuads :
			return True, self.DctDefQuads[ p_ident ]
		else :
			return False, None


def Object_Obj4Gui_Is_Container( o ):
	return isinstance( o, Obj4Gui__Container)

def Obj4GuiTyp_Is_Complex( typ ):
	t_Bool = typ in [ Obj4GuiTyp.DefQuad, Obj4GuiTyp.UseQuad, Obj4GuiTyp.DataTray, Obj4GuiTyp.DataForm, Obj4GuiTyp.DataSheet ]
	return t_Bool

def Object_Obj4Gui_Is_Placable( o ):
	# t_Bool = typ in [ Obj4GuiTyp.Button, Obj4GuiTyp.ComboBox, Obj4GuiTyp.Label, Obj4GuiTyp.ListBox, Obj4GuiTyp.TabSet, Obj4GuiTyp.TextBox ]
	return isinstance( o, Obj4Gui__Generic_Placable) or isinstance( o, Obj4Gui__Container_Placable)

# ==================================================
# XML handling and Obj4Gui definition assemblage
# --------------------------------------------------

def mdf_Make_Obj4Gui_IntoPlace_FromTagAndVal( p_Obj4Data_DataBase, p_Meta_Obj4Gui, p_ContainerObj4Gui, p_i, p_new_tag, p_new_val):
	dprint( "mdf_Make_Obj4Gui_IntoPlace_FromTagAndVal" )
	dprint( "p_ContainerObj4Gui isTyp: " + str(p_ContainerObj4Gui.isTyp) )
	i_tagtyp = MdfTag_EnumOfString( p_new_tag )
	dprint( "i_tagtyp:" + str( i_tagtyp))
	r_didok = False
	r_PyObj = None
	if i_tagtyp == MdfmlTag.NoTag :
		dprint( "Defining:" + "NoTag")
	elif i_tagtyp == MdfmlTag.mdfml :
		dprint( "Hi. I shouldn't be here!! mdfml tag should only be outermost.")
		#input( "Press ENTER")
	# ---- the interface structures
	elif i_tagtyp == MdfmlTag.window :
		r_PyObj = Obj4Gui_Window( p_new_val )
		p_Meta_Obj4Gui.DctWindows_lnk_Obj( r_PyObj, p_i )
		r_didok = True
		#dprint( "Defined!:" + "Obj4Gui_Window abd added to the Dictionary which now has:" + str(len(p_dct_Obj4Gui)))
	elif i_tagtyp == MdfmlTag.defquad :
		#dprint( "Not Yet Defining:" + "defquad")
		#pass
		r_PyObj = Obj4Gui_DefQuad( p_i )
		p_ContainerObj4Gui.add_SubObj( r_PyObj )
		r_didok = True
		dprint( "Defined!:" + "Obj4Gui_DefQuad")
	elif i_tagtyp == MdfmlTag.usequad :
		#dprint( "Not Yet Defining:" + "usequad")
		r_PyObj = Obj4Gui_UseQuad( p_i )
		p_ContainerObj4Gui.add_SubObj( r_PyObj )
		r_didok = True
		dprint( "Defined!:" + "Obj4Gui_UseQuad")
		dprint( "Now trying to set use for the referenced quad." + p_new_val )
		t_got_dq_ok, t_dq_PyObj = p_Meta_Obj4Gui.get_DefQuad_by_Ident(p_new_val)
		if t_got_dq_ok :
			dprint( "Adding referenced quad as a sub Object.")
			r_PyObj.add_SubObj( t_dq_PyObj )
	elif i_tagtyp == MdfmlTag.datatray :
		#dprint( "Not Yet Defining:" + "datatray")
		r_PyObj = Obj4Gui_DataTray( p_i )
		p_ContainerObj4Gui.add_SubObj( r_PyObj )
		r_didok = True
		dprint( "Defined!:" + "Obj4Gui_DataTray")
	elif i_tagtyp == MdfmlTag.dataform :
		#dprint( "Not Yet Defining:" + "dataform")
		r_PyObj = Obj4Gui_DataForm( p_i )
		if False and p_ContainerObj4Gui.isTyp == Obj4GuiTyp.DataTray :
			p_ContainerObj4Gui.Set_Obj_4Frm( r_PyObj )
		else :
			p_ContainerObj4Gui.add_SubObj( r_PyObj )
		r_didok = True
		dprint( "Defined!:" + "Obj4Gui_DataForm")
	elif i_tagtyp == MdfmlTag.datasheet :
		#dprint( "Not Yet Defining:" + "datasheet")
		r_PyObj = Obj4Gui_DataSheet( p_i )
		if p_ContainerObj4Gui.isTyp == Obj4GuiTyp.DataTray : # just making sure we don't call an invalid object method
			p_ContainerObj4Gui.Set_Obj_4Sht( r_PyObj )
		p_ContainerObj4Gui.add_SubObj( r_PyObj )
		r_didok = True
		dprint( "Just defined a Datasheet object." )
		# input( "Press ENTER")
	# ---- the Cross linking
	elif i_tagtyp == MdfmlTag.one_uid :
		dprint( "Processing a one_uid")
		if len( p_new_val ) > 0 :
			if p_ContainerObj4Gui.isTyp == Obj4GuiTyp.DefQuad :
				t_linked_dq_ok = p_Meta_Obj4Gui.DctDefQuads_lnk_Obj_Ident( p_ContainerObj4Gui, p_new_val )
				if t_linked_dq_ok :
					dprint( "Added a link to a DefQuad in the Meta object's dictionary")
	# ---- the Controls
	elif i_tagtyp == MdfmlTag.ctrl_button :
		r_PyObj = Obj4Gui_Button( p_i )
		r_PyObj.Button_text = p_new_val
		p_ContainerObj4Gui.add_SubObj( r_PyObj )
		r_didok = True
		dprint( "Defined!:" + "Obj4Gui_Button")
	elif i_tagtyp == MdfmlTag.ctrl_checkbox :
		r_PyObj = Obj4Gui_Checkbutton( p_i )
		p_ContainerObj4Gui.add_SubObj( r_PyObj )
		r_didok = True
		dprint( "Defined!:" + "Obj4Gui_Checkbutton")
	elif i_tagtyp == MdfmlTag.ctrl_combobox :
		r_PyObj = Obj4Gui_Combobox( p_i )
		p_ContainerObj4Gui.add_SubObj( r_PyObj )
		r_didok = True
		dprint( "Defined!:" + "Obj4Gui_Combobox")
	elif i_tagtyp == MdfmlTag.ctrl_label :
		r_PyObj = Obj4Gui_Label( p_i )
		p_ContainerObj4Gui.add_SubObj( r_PyObj )
		r_didok = True
		dprint( "Defined!:" + "Obj4Gui_Label")
	elif i_tagtyp == MdfmlTag.ctrl_listbox :
		r_PyObj = Obj4Gui_Listbox( p_i )
		p_ContainerObj4Gui.add_SubObj( r_PyObj )
		r_didok = True
		dprint( "Defined!:" + "Obj4Gui_Listbox")
	elif i_tagtyp == MdfmlTag.ctrl_textbox :
		#dprint( "Not Yet Defining:" + "ctrl_textbox")
		r_PyObj = Obj4Gui_TextBox( p_i )
		p_ContainerObj4Gui.add_SubObj( r_PyObj )
		r_didok = True
		dprint( "Defined!:" + "Obj4Gui_TextBox")
	elif i_tagtyp == MdfmlTag.ctrl_subtab :
		# dprint( "Not Yet Defining:" + "ctrl_subtab")
		r_PyObj = Obj4Gui_SubTab( p_i )
		p_ContainerObj4Gui.add_SubObj( r_PyObj )
		r_didok = True
		dprint( "Defined!:" + "Obj4Gui_SubTab")
	elif i_tagtyp == MdfmlTag.ctrl_tabset :
		# dprint( "Not Yet Defining:" + "ctrl_tabset")
		r_PyObj = Obj4Gui_TabSet( p_i )
		p_ContainerObj4Gui.add_SubObj( r_PyObj )
		r_didok = True
		dprint( "Defined!:" + "Obj4Gui_TabSet")
	# ---- other features
	elif i_tagtyp == MdfmlTag.form_mode :
		dprint( "Not Yet Defining:" + "form_mode")
	#elif i_tagtyp == MdfmlTag.link_pair :
	#	dprint( "Not Yet Defining:" + "link_pair")
	#elif i_tagtyp == MdfmlTag.one_busform_datacol :
	#	dprint( "Not Yet Defining:" + "one_busform_datacols")
	elif i_tagtyp == MdfmlTag.one_caption :
		dprint( "Defining:" + "one_caption")
		if p_ContainerObj4Gui.isTyp == Obj4GuiTyp.Button :
			p_ContainerObj4Gui.Button_text = p_new_val
		elif p_ContainerObj4Gui.isTyp == Obj4GuiTyp.Label :
			p_ContainerObj4Gui.Label_text = p_new_val
		r_didok = True
	#elif i_tagtyp == MdfmlTag.one_click_actionref :
	#	dprint( "Not Yet Defining:" + "one_click_actionref")
	#elif i_tagtyp == MdfmlTag.one_click_actiontype :
	#	dprint( "Not Yet Defining:" + "one_click_actiontype")
	#elif i_tagtyp == MdfmlTag.one_ctrl :
	#	dprint( "Not Yet Defining:" + "one_ctrl")
	#elif i_tagtyp == MdfmlTag.one_datacol :
	#	dprint( "Not Yet Defining:" + "one_datacol")
	#elif i_tagtyp == MdfmlTag.one_datacol_name :
	#	dprint( "Not Yet Defining:" + "one_datacol_name")
	#elif i_tagtyp == MdfmlTag.one_default_dataref :
	#	dprint( "Not Yet Defining:" + "one_default_dataref")
	#elif i_tagtyp == MdfmlTag.one_default_form_mode :
	#	dprint( "Not Yet Defining:" + "one_default_form_mode")
	elif i_tagtyp == MdfmlTag.one_name :
		dprint( "Defining:" + "one_name" + " into .name :" + p_new_val)
		p_ContainerObj4Gui.name = p_new_val
	#elif i_tagtyp == MdfmlTag.one_subform_datacol :
	#	dprint( "Not Yet Defining:" + "one_subform_datacols")
	#elif i_tagtyp == MdfmlTag.one_uform_datalink :
	#	dprint( "Not Yet Defining:" + "one_uform_datalink")
	#elif i_tagtyp == MdfmlTag.see_datacol_name :
	#	dprint( "Not Yet Defining:" + "see_datacol_name")
	elif i_tagtyp == MdfmlTag.subdiv :
		dprint( "Defining:" + "subdiv")
		r_PyObj = Obj4Gui_Frame( p_i )
		p_ContainerObj4Gui.add_SubObj( r_PyObj )
		r_didok = True
		dprint( "Defined!:" + "Obj4Gui_Frame")
	elif i_tagtyp == MdfmlTag.place_x :
		dprint( "Defining:" + "place_x")
		if Object_Obj4Gui_Is_Placable( p_ContainerObj4Gui )  :
			p_ContainerObj4Gui.set_place_x( p_new_val )
	elif i_tagtyp == MdfmlTag.place_y :
		dprint( "Defining:" + "place_y")
		if Object_Obj4Gui_Is_Placable( p_ContainerObj4Gui )  :
			p_ContainerObj4Gui.set_place_y( p_new_val )
	elif i_tagtyp == MdfmlTag.place_w :
		dprint( "Defining:" + "place_w")
		if Object_Obj4Gui_Is_Placable( p_ContainerObj4Gui )  :
			p_ContainerObj4Gui.set_place_w( p_new_val )
	elif i_tagtyp == MdfmlTag.place_h :
		dprint( "Defining:" + "place_h")
		if Object_Obj4Gui_Is_Placable( p_ContainerObj4Gui )  :
			p_ContainerObj4Gui.set_place_h( p_new_val )
	else:
		dprint( "UnDefined !" )
	return r_didok, r_PyObj

# --------------------------------------------------
# XML handling and Data definition assemblage
# --------------------------------------------------

def mdf_Make_X2O4D_FromTagAndVal( p_x2o4d, p_i, p_new_tag, p_new_val):
	dprint( "mdf_Make_X2O4D_FromTagAndVal" )
	i_tagtyp = MdfTag_EnumOfString( p_new_tag )
	dprint( "i_tagtyp:" + str( i_tagtyp))
	r_x2o4d = p_x2o4d
	r_be_in_datamode = False
	r_did_new_x2obj = False
	if i_tagtyp == MdfmlTag.NoTag :
		dprint( "Defining:" + "NoTag")
		dprint( "Hi. I shouldn't be here!! NoTag type should only have a temp presence.")
		#input( "Press ENTER")
	elif i_tagtyp == MdfmlTag.mdfml :
		dprint( "Hi. I shouldn't be here!! mdfml tag shouldn't reappear.")
		#input( "Press ENTER")
	# ---- the interface structures
	elif i_tagtyp == MdfmlTag.dataref :
		dprint( "Defining:" + "dataref")
		r_be_in_datamode = True
		r_x2o4d = Xml2Obj4Data( Obj4DataTyp.O4D_DataRef)
		r_did_new_x2obj = True
	elif i_tagtyp == MdfmlTag.one_uid :
		r_be_in_datamode = True
		p_x2o4d.add_property( i_tagtyp, p_new_val )
	elif i_tagtyp == MdfmlTag.one_name :
		r_be_in_datamode = True
		p_x2o4d.add_property( i_tagtyp, p_new_val )
	elif i_tagtyp == MdfmlTag.rstref :
		dprint( "Adding rstref as a property")
		r_be_in_datamode = True
		p_x2o4d.add_property( i_tagtyp, p_new_val )
		dprint( "Key:" + str( i_tagtyp ) + " Tag:" + p_new_tag + " Val:" +  p_new_val )
		#input( "Press ENTER")
	return r_be_in_datamode, r_did_new_x2obj, r_x2o4d

def mdf_Make_Obj4Data_IntoPlace_FromTagAndVal( p_Obj4Data_DataBase, p_x2o4d, p_i ):
	# use the x2o4d to make the guiding Obj4Data structures
	# if title seems strange it's because the Tag and Value pairs from the XML have individually
	# gone into the p_x2o4d object and now from the back recursion point we will pull them back out
	# and use them to populate a data guidance object (that will then be independent of the XML )
	# which means that the p_i it gets passed in from the dataref tag
	#dprint( "mdf_Make_Obj4Data_IntoPlace_FromTagAndVal" + " p_i=" + str( p_i) )
	#dprint( "Inspecting p_x2o4d")
	#dprint( " p_x2o4d.typ = " + str( p_x2o4d.typ) )
	#dprint( " p_x2o4d prop has " + str(len( p_x2o4d.prop )) + " items") 
	i_rstref = ""
	#i_title = ""
	#i_uid = ""
	for k in p_x2o4d.prop.keys() :
		#dprint( "k:" + str(k)  + " MdfmlTag.rstref " + str(MdfmlTag.rstref) )
		if k == MdfmlTag.rstref :
			i_rstref = p_x2o4d.prop[ k ]
			#dprint( "found rstref: " + i_rstref )
		#elif k == MdfmlTag.one_uid :
		#	i_uid = p_x2o4d.prop[ k ]
		#elif k == MdfmlTag.one_name :
		#	i_title = p_x2o4d.prop[ k ]
		#dprint( " p_x2o4d prop key:" + str(k) + " val:" + p_x2o4d.prop[ k ] )
	#dprint( "Will need " + str( p_x2o4d.typ ) + " to equal " + str( Obj4DataTyp.O4D_DataRef ) )
	#input( "Press ENTER")
	if ( p_x2o4d.typ == Obj4DataTyp.O4D_DataRef ) and len( i_rstref ) > 0 : # and len( i_uid ) > 0 
		# make a new DataRef object
		i_DataRef = Obj4Data_DataRef( p_i )
		i_DataRef.set_rstref( i_rstref )
		dprint( "Adding rstref: " + i_rstref + " to a DataRef")
		#input( "Press ENTER")
		p_Obj4Data_DataBase.set_DataRef( i_DataRef )


# --------------------------------------------------
# XML traversal
# --------------------------------------------------

def mdf_AssembleInterims_ByTraversing_XML( p_Obj4Data_DataBase, p_Meta_Obj4Gui, p_1st_level_element, p_cntrid, p_prnt_elemnt ):
	# this should be pass the series of XML tags just below the mdfml tag
	# p_1st_level_element is an XML element
	# p_cntrid is just a counter number object
	def mdf_Recursive_Traversal_XML( p_Obj4Gui, p_x2o4d, p_elemnt, p_depth, p_prnt_elemnt, p_be_in_datamode ):
		# Note: listof1_i is an array of one element just to get around the stupidity of Python's immutable scalars
		dprint("mdf_Recursive_Traversal_XML" + " Depth:" + str(p_depth) + " i:" + str( p_cntrid.Get() ) )
		# increment for a new node ID and new depth
		# nonlocal p_cntrid
		p_cntrid.Incr()
		i_ThisTag_i = p_cntrid.Get()
		r_ok = False
		i_depth = p_depth + 1
		if p_x2o4d is None :
			dprint( "Passed a None p_x2o4d" )
		else :
			dprint( "Passed p_x2o4d of type:" + str(p_x2o4d.typ))
		i_x2o4d = p_x2o4d
		# get from element 
		dprint( p_elemnt.tag)
		dprint( p_elemnt.text)
		elmnt_tag = p_elemnt.tag
		if p_elemnt.text is None :
			elmnt_val = ""
		else :
			elmnt_val = p_elemnt.text.strip()
		dprint( "Node: " + str(p_cntrid.Get()) + " Tag:" + elmnt_tag + " Val:" +  elmnt_val)
		i_be_in_datamode = False
		if elmnt_tag in MdfTag_GetList():
			if p_be_in_datamode or MdfTag_TagStr_IsDataRef( elmnt_tag) :
				p_subof_tag = p_prnt_elemnt.text.strip()
				i_be_in_datamode, i_did_new_x2obj, t_x2o4d = mdf_Make_X2O4D_FromTagAndVal( p_x2o4d, i_ThisTag_i, elmnt_tag, elmnt_val)
				if i_did_new_x2obj :
					i_x2o4d = t_x2o4d
				r_ok = True
			elif MdfTag_TagStr_IsGuiObj( elmnt_tag) :
				# create a new object from the XML tag, chaining to the current object, adding to dictionary if a window
				r_ok, i_Obj4Gui = mdf_Make_Obj4Gui_IntoPlace_FromTagAndVal( p_Obj4Data_DataBase, p_Meta_Obj4Gui, p_Obj4Gui, p_cntrid.Get(), elmnt_tag, elmnt_val)
			else :
				dprint( "  Unknown Tag category!")
				#input( "Press ENTER")
		else :
			dprint( "  Unknown Tag!")
			#input( "Press ENTER")
		if False: # not currently using XML attributes in the design but tbis where we would
			# do any attributes
			dprint( "  Attributes")
			for a_k in p_elemnt.attrib.keys():
				a_v = p_elemnt.attrib.get( a_k, "")
				# dprint( "  Node: " + str(listof1_i[0]) + " A: Key: " + a_k + " Value: " + a_v)
		# do any sub-elements
		dprint( "  Traversal recursing:")
		if ( p_be_in_datamode or i_be_in_datamode ) and len( p_elemnt ) > 0 :
			# if we're processing data def tags then carry through the same Obj4Gui
			for i_sub_e in p_elemnt:
				r_ok = mdf_Recursive_Traversal_XML( p_Obj4Gui, i_x2o4d, i_sub_e, i_depth, p_elemnt, True )
			# tail recursion point
			mdf_Make_Obj4Data_IntoPlace_FromTagAndVal( p_Obj4Data_DataBase, i_x2o4d, i_ThisTag_i )
			# if we just made a DataRef then we need to add its idnum to the DataTray p_prnt_elemnt
			if ( i_x2o4d.typ == Obj4DataTyp.O4D_DataRef ) and ( p_Obj4Gui.isTyp == Obj4GuiTyp.DataTray ) :
				p_Obj4Gui.Set_DataRef_id( i_ThisTag_i )
				dprint( "p_Obj4Gui.Set_DataRef_id " + str( i_ThisTag_i ) )
		elif r_ok :
			# here we're processing into making deeper Obj4Gui
			for i_sub_e in p_elemnt:
				r_ok = mdf_Recursive_Traversal_XML( i_Obj4Gui, i_x2o4d, i_sub_e, i_depth, p_elemnt, False )
		return r_ok
	dprint("mdf_AssembleInterims_ByTraversing_XML")
	r_ok = False
	p_Obj4Gui = Obj4Gui_Root(-1)
	r_ok = mdf_Recursive_Traversal_XML( p_Obj4Gui, None, p_1st_level_element, 0, p_prnt_elemnt, False )
	return r_ok

def mdf_Traverse_Contents_MDFML_MakingMetaObject( p_mdfml_elemnt):
	dprint( "mdf_Traverse_Contents_MDFML_MakingMetaObject")
	r_ok = False
	cntrid = IdCounter()
	r_Obj4Data_DataBase = Obj4Data_DataBase( Obj4Data_DataBaseTyp.Sqlite )
	r_Meta_Obj4Gui = Obj4Gui_Meta()
	dprint( "  Traversing the MDFML to build dictionaries")
	for sub_e in p_mdfml_elemnt:
		r_ok = mdf_AssembleInterims_ByTraversing_XML( r_Obj4Data_DataBase, r_Meta_Obj4Gui, sub_e, cntrid, p_mdfml_elemnt )
	return r_ok, r_Meta_Obj4Gui, r_Obj4Data_DataBase

# switch point between old and new methods

def mdf_Traverse_MDFML( p_elemnt ):
	# This layer just checks we have an MDFML tag and if so, passes to another layer for processing
	dprint( "mdf_Traverse_MDFML")
	dprint( p_elemnt.tag)
	r_ok = False
	if p_elemnt.tag == MdfTag_EnumValueString( MdfmlTag.mdfml ) :
		r_ok, r_Meta_Obj4Gui, r_Obj4Data_DataBase = mdf_Traverse_Contents_MDFML_MakingMetaObject( p_elemnt)
	else :
		r_dct_Obj4Gui = {} # just an empty for return
		dprint( "  Not the expected tag mdfml")
	return r_ok, r_Meta_Obj4Gui, r_Obj4Data_DataBase

def mdf_Process_XML_File( p_xml_fileref):
	dprint("mdf_Process_XML_File")
	dprint("File: " + p_xml_fileref )
	i_tree = im_ET.parse( p_xml_fileref)
	i_root = i_tree.getroot()
	r_ok, r_Meta_Obj4Gui, r_Obj4Data_DataBase = mdf_Traverse_MDFML( i_root )
	return r_ok, r_Meta_Obj4Gui, r_Obj4Data_DataBase

# --------------------------------------------------
# GUI Tkinter Object building
# Use the assembled TkObj tree to build actual Tkinter objects 
# --------------------------------------------------

# Prefix guide
# dtk = definition Tkinter elements
# rtk = run-time Tkinter elements

# For now, handle the connections between design time objects and run-time objects with a
# simple dictionary of dictionaries

@unique
class rtk_Tktyp(Enum):
	rtk_Tktyp_TreeView = auto()
	rtk_Tktyp_Frame4Form = auto()
	rtk_Tktyp_Frame4Sheet = auto()

def rtk_TktypStr( p_typ ):
	if p_typ == rtk_Tktyp.rtk_Tktyp_TreeView :
		return "TreeView4DataSheet"
	elif p_typ == rtk_Tktyp.rtk_Tktyp_Frame4Form :
		return "Frame4Form"
	elif p_typ == rtk_Tktyp.rtk_Tktyp_Frame4Sheet :
		return "Frame4Sheet"
	else :
		return "Unknown"

def rtk_For_ObjId_Set_TkObjRef( p_Dct_rtk, p_ObjId, p_TypId, p_TkObjRef ):
	# quite simple, for the dictionary p_Dct_rtk set p_ObjId, p_TypId p_TkObjRef
	dprint( "rtk_For_ObjId_Set_TkObjRef")
	if not p_ObjId in p_Dct_rtk :
		p_Dct_rtk[ p_ObjId ] = {}
		dprint("Made new dictionary for object id " + str(p_ObjId) )
	p_Dct_rtk[ p_ObjId ][ p_TypId ] = p_TkObjRef
	dprint("Set dictionary for object id " + str(p_ObjId) + " key: " + str(p_TypId) + " as p_TkObjRef" )
	#input( "Press ENTER")

def rtk_For_ObjId_Get_TkObjRef( p_Dct_rtk, p_ObjId, p_TypId ):
	# quite simple, for the dictionary p_Dct_rtk set p_ObjId, p_TypId p_TkObjRef
	dprint( "rtk_For_ObjId_Get_TkObjRef" + "p_ObjId: " + str(p_ObjId) + " p_TypId: " + str(p_TypId) + " " + rtk_TktypStr( p_TypId ))
	print( p_Dct_rtk )
	if p_ObjId in p_Dct_rtk :
		dprint( "Yes for: p_ObjId in p_Dct_rtk")
		if p_TypId in p_Dct_rtk[ p_ObjId ] :
			dprint( "Yes for: p_TypId in p_Dct_rtk[ p_ObjId ]")
			return p_Dct_rtk[ p_ObjId ][ p_TypId ]
		else :
			dprint( "No for: p_TypId in p_Dct_rtk[ p_ObjId ]")
			return None
	else :
		dprint( "No for: p_ObjId in p_Dct_rtk")
		return None

# --------------------------------------------------
# Tkinter Live actions
# --------------------------------------------------

# putting this in place as archetype for future use
def rtk_DataSheet_Populate_From_QueryRef( p_tk_trvw, p_Obj4Data_DataBase, p_rstref ):
	dprint( "rtk_DataSheet_Populate_From_QueryRef" )
	# need to get the db filename from the p_Obj4Data_DataBase object
	i_db_filename = p_Obj4Data_DataBase.get_db_filename()
	# i_db_filename = ExampleDetail_Database_Filename()
	a_didok, a_rows, a_names = SqLite_Filename_GetTable_Rows( i_db_filename, p_rstref )
	if a_didok :
		p_tk_trvw["column"] = a_names
		p_tk_trvw["show"] = "headings"
		for column in p_tk_trvw["columns"]:
			p_tk_trvw.heading(column, text=column)
		for row in a_rows:
			p_tk_trvw.insert("", "end", values=row)
	else :
		pass

def colour_bg_safely():
	return "PaleGreen1"

def colour_bg_neutral():
	return "LightSteelBlue1"

def colour_bg_spacer():
	return "ivory2"

def colour_bg_datatray():
	return "wheat1"

def colour_bg_dataform():
	return "azure"

def colour_bg_datasheet():
	return "lavender"


def tk_pack_pad():
	return 4

def mdf_Tkinter_Build_Widget( p_Tk_HolderObj, p_Obj4Gui):
	dprint( "mdf_Tkinter_Build_Widget" )
	i_typ = p_Obj4Gui.isTyp
	dprint( "i_typ: " + str(i_typ) + Obj4GuiTyp_Str( i_typ ) )
	r_newsub = False
	r_new_Tk_object = None
	# order doesn't matter, but doing these alphabetic for easy checking
	# as this is about Tkinter elements, tbe names match Tkinter concepts 
	if i_typ == Obj4GuiTyp.Button :
		dprint( "Making a Button" )
		r_new_Tk_object = im_tkntr.Button( p_Tk_HolderObj, text=p_Obj4Gui.Button_text ) 
			# , command=on_button_exit)
		if p_Obj4Gui.unplaced():
			dprint( "Packing Button !!!!!!!!!!!!!!!!!" )
			r_new_Tk_object.pack( side=im_tkntr.LEFT, padx=tk_pack_pad(), pady=tk_pack_pad())
		else:
			dprint( "Placing Button" )
			r_new_Tk_object.place( x=p_Obj4Gui.place_x, y=p_Obj4Gui.place_y, width=p_Obj4Gui.place_w, height=p_Obj4Gui.place_h)
		r_newsub = True
		dprint( "Made a Button: " + p_Obj4Gui.Button_text )
	elif i_typ == Obj4GuiTyp.CheckBox :
		dprint( "Making a Checkbutton" )
		r_new_Tk_object = im_tkntr.Checkbutton( p_Tk_HolderObj, text=p_Obj4Gui.Checkbutton_text ) # "Auto Link", \
			# variable=t3_chkvr_autolink, command = t3_on_chkbx_autolink)
		if p_Obj4Gui.unplaced():
			dprint( "Packing Checkbutton !!!!!!!!!!!!!!!!!" )
			r_new_Tk_object.pack( side=im_tkntr.LEFT ) # , anchor=im_tkntr.W, expand=im_tkntr.YES)
		else:
			dprint( "Placing Checkbutton -------" +str(p_Obj4Gui.place_x) +"," +str(p_Obj4Gui.place_y) +"," +str(p_Obj4Gui.place_w) +"," +str(p_Obj4Gui.place_h))
			r_new_Tk_object.place( x=p_Obj4Gui.place_x, y=p_Obj4Gui.place_y, width=p_Obj4Gui.place_w, height=p_Obj4Gui.place_h)
		r_newsub = True
		dprint( "Made a Checkbutton: " + p_Obj4Gui.Checkbutton_text )
	elif i_typ == Obj4GuiTyp.ComboBox :
		dprint( "Making a Combobox" )
		r_new_Tk_object = im_tkntr.ttk.Combobox( p_Tk_HolderObj ) # , width = 20, textvariable = t2_var_link_chld) 
		if p_Obj4Gui.unplaced():
			dprint( "Packing Combobox !!!!!!!!!!!!!!!!!" )
			r_new_Tk_object.pack( side = im_tkntr.LEFT ) #, fill = im_tkntr.X, expand=1 )
		else:
			dprint( "Placing Combobox -------" +str(p_Obj4Gui.place_x) +"," +str(p_Obj4Gui.place_y) +"," +str(p_Obj4Gui.place_w) +"," +str(p_Obj4Gui.place_h))
			r_new_Tk_object.place( x=p_Obj4Gui.place_x, y=p_Obj4Gui.place_y, width=p_Obj4Gui.place_w, height=p_Obj4Gui.place_h)
		r_newsub = True
	elif i_typ == Obj4GuiTyp.TextBox :
		dprint( "Making a Entry" )
		r_new_Tk_object = im_tkntr.Entry( p_Tk_HolderObj ) #, textvariable=t2_var_file_dbs)
		if p_Obj4Gui.unplaced():
			dprint( "Packing Entry !!!!!!!!!!!!!!!!!" )
			r_new_Tk_object.pack( side = im_tkntr.LEFT) # , fill = im_tkntr.X, expand=1, padx=tk_pack_pad(), pady=tk_pack_pad()) 
		else:
			dprint( "Placing Entry -------" +str(p_Obj4Gui.place_x) +"," +str(p_Obj4Gui.place_y) +"," +str(p_Obj4Gui.place_w) +"," +str(p_Obj4Gui.place_h))
			r_new_Tk_object.place( x=p_Obj4Gui.place_x, y=p_Obj4Gui.place_y, width=p_Obj4Gui.place_w, height=p_Obj4Gui.place_h)
		r_newsub = True
	elif i_typ == Obj4GuiTyp.Frame :
		dprint( "Making a Frame" )
		r_new_Tk_object = im_tkntr.Frame( p_Tk_HolderObj, bg=colour_bg_neutral())
		if p_Obj4Gui.unplaced():
			dprint( "Packing Frame !!!!!!!!!!!!!!!!!" )
			r_new_Tk_object.pack( ) # fill = im_tkntr.X
		else:
			dprint( "Placing Frame -------" +str(p_Obj4Gui.place_x) +"," +str(p_Obj4Gui.place_y) +"," +str(p_Obj4Gui.place_w) +"," +str(p_Obj4Gui.place_h))
			r_new_Tk_object.place( x=p_Obj4Gui.place_x, y=p_Obj4Gui.place_y, width=p_Obj4Gui.place_w, height=p_Obj4Gui.place_h)
		r_newsub = True
	elif i_typ == Obj4GuiTyp.Label :
		dprint( "Making a Label" )
		r_new_Tk_object = im_tkntr.Label( p_Tk_HolderObj, text= p_Obj4Gui.Label_text )
		if p_Obj4Gui.unplaced():
			dprint( "Packing Label !!!!!!!!!!!!!!!!!" )
			r_new_Tk_object.pack( side = im_tkntr.LEFT, padx=tk_pack_pad(), pady=tk_pack_pad())
		else:
			dprint( "Placing Label -------" +str(p_Obj4Gui.place_x) +"," +str(p_Obj4Gui.place_y) +"," +str(p_Obj4Gui.place_w) +"," +str(p_Obj4Gui.place_h))
			r_new_Tk_object.place( x=p_Obj4Gui.place_x, y=p_Obj4Gui.place_y, width=p_Obj4Gui.place_w, height=p_Obj4Gui.place_h)
		r_newsub = True
	#elif i_typ == Obj4GuiTyp.LabelFrame :
	#	dprint( "Not Yet Making a LabelFrame" )
	elif i_typ == Obj4GuiTyp.ListBox :
		dprint( "Making a Listbox" )
		r_new_Tk_object = im_tkntr.Listbox( p_Tk_HolderObj ) # , height=2 )
		if p_Obj4Gui.unplaced():
			dprint( "Packing Listbox !!!!!!!!!!!!!!!!!" )
			r_new_Tk_object.pack( side = m_tkntr.LEFT) # , fill=m_tkntr.BOTH, expand=True)
		else:
			dprint( "Placing Listbox -------" +str(p_Obj4Gui.place_x) +"," +str(p_Obj4Gui.place_y) +"," +str(p_Obj4Gui.place_w) +"," +str(p_Obj4Gui.place_h))
			r_new_Tk_object.place( x=p_Obj4Gui.place_x, y=p_Obj4Gui.place_y, width=p_Obj4Gui.place_w, height=p_Obj4Gui.place_h)
		# r_new_Tk_object.bind('<Double-Button>',  lambda x: t1_on_dblclck_selected_disposal() )
		r_newsub = True
	elif i_typ == Obj4GuiTyp.ScrolledText :
		dprint( "Not Yet Making a ScrolledText" )
		#st_rslts = im_tkntr_tkst.ScrolledText( tab_6)
		#st_rslts.pack( side=im_tkntr.LEFT, fill=im_tkntr.BOTH, expand=True )
	elif i_typ == Obj4GuiTyp.SubTab :
		dprint( "Making a SubTab" )
		r_new_Tk_object = im_tkntr.Frame( p_Tk_HolderObj)
		p_Tk_HolderObj.add( r_new_Tk_object, text="NewTab")
		r_newsub = True
	elif i_typ == Obj4GuiTyp.TabSet :
		dprint( "Making a Tabset" )
		r_new_Tk_object = im_tkntr_ttk.Notebook( p_Tk_HolderObj)
		if p_Obj4Gui.unplaced():
			dprint( "Packing Notebook !!!!!!!!!!!!!!!!!" )
			r_new_Tk_object.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True) # expand=1, fill=im_tkntr.BOTH
		else:
			dprint( "Placing Notebook -------" +str(p_Obj4Gui.place_x) +"," +str(p_Obj4Gui.place_y) +"," +str(p_Obj4Gui.place_w) +"," +str(p_Obj4Gui.place_h))
			r_new_Tk_object.place( x=p_Obj4Gui.place_x, y=p_Obj4Gui.place_y, width=p_Obj4Gui.place_w, height=p_Obj4Gui.place_h)
		r_newsub = True
	#elif i_typ == Obj4GuiTyp.Treeview :
	#	dprint( "Not Yet Making a Treeview" )
		#r_new_Tk_object = im_tkntr_ttk.Treeview( tk_frame_prnt_data)
		#r_new_Tk_object.place( relheight=1.0, relwidth=1.0)
	else :
		dprint( "Not Making Any Widget!!" )
	return r_newsub, r_new_Tk_object


class ForTkLiveCallback:
    def __init__(self, func, *args, **kwargs):
        self.func = func
        self.args = args
        self.kwargs = kwargs
    def __call__(self):
        self.func(*self.args, **self.kwargs)

def mdf_Tkinter_TrayFunction_RstLoad( p_dct_rtk, p_Obj4Data_DataBase, p_Obj4Gui ):
	dprint( "mdf_Tkinter_TrayFunction_RstLoad")
	# confirm this is for a DataTray
	i_typ = p_Obj4Gui.isTyp
	if i_typ == Obj4GuiTyp.DataTray :
		dprint( "looking good, is a DataTray")
		# by the time call gets enacted, there should be two child objects of the DataTray, a DataForm and a Datasheet
		# for now, we'll just handle the datasheet
		if p_Obj4Gui.Has_DataRef_id( ):
			i_dref_id = p_Obj4Gui.Get_DataRef_id()
			i_Obj_4Rst = p_Obj4Data_DataBase.get_DataRef( i_dref_id ) # p_Obj4Gui.Get_Obj_4Rst()
			i_rstref = i_Obj_4Rst.get_rstref()
			if p_Obj4Gui.Has_Obj_4Frm( ):
				i_dataform = p_Obj4Gui.Get_Obj_4Frm()
				dprint( "ok, but not yet handling the DataForm")
			if p_Obj4Gui.Has_Obj_4Sht( ):
				i_o4g_datasheet = p_Obj4Gui.Get_Obj_4Sht()
				i_o4g_datasheet_isTyp = i_o4g_datasheet.isTyp
				if i_o4g_datasheet_isTyp == Obj4GuiTyp.DataSheet :
					dprint( "ok, we have a Datasheet for this DataTray")
					i_TreeView = rtk_For_ObjId_Get_TkObjRef( p_dct_rtk, i_o4g_datasheet.id, rtk_Tktyp.rtk_Tktyp_TreeView )
					rtk_DataSheet_Populate_From_QueryRef( i_TreeView, p_Obj4Data_DataBase, i_rstref )
				else :
					dprint( "oops, got DataSheet object but is not a DataSheet ?")
			else :
				dprint( "oops, there was no DataSheet for this DataTray")
		else :
			dprint( "oops, there was no Rst reference for this DataTray")
	else :
		dprint( "oops, this should only be called for a DataTray")
	# no return as this is just an action request

def mdf_Tkinter_Build_Complex( p_dct_rtk, p_Tk_HolderObj, p_Obj4Data_DataBase, p_Obj4Gui, p_Prnt_Obj4Gui):
	dprint( "mdf_Tkinter_Build_Complex" )
	i_typ = p_Obj4Gui.isTyp
	dprint( "p_Obj4Gui.isTyp: " + str(i_typ) + Obj4GuiTyp_Str( i_typ ) )
	r_newsub = False
	r_new_Tk_object = None
	if i_typ == Obj4GuiTyp.DefQuad :
		dprint( "Now passing down from a DefQuad" )
		r_newsub = True
		r_new_Tk_object = p_Tk_HolderObj
	elif i_typ == Obj4GuiTyp.UseQuad :
		if p_Prnt_Obj4Gui.isTyp == Obj4GuiTyp.Window :
			dprint( "Just passing through a UseQuad" )
			r_newsub = True
			r_new_Tk_object = p_Tk_HolderObj
		elif p_Prnt_Obj4Gui.isTyp == Obj4GuiTyp.DataForm :
			dprint( "Puttin in a UseQuad as a subform" )
			r_new_Tk_object = im_tkntr.LabelFrame( p_Tk_HolderObj, text="DataSheet")
			r_new_Tk_object.pack( ) # fill="both", expand="yes"  
			r_newsub = True
		else :
			dprint( "No context for a UseQuad" )
	elif i_typ == Obj4GuiTyp.DataTray :
		dprint( "Now making a Frame for the DataTray" )
		if False:
			dprint( "Now just using p_Tk_HolderObj for the DataTray" )
			r_new_Tk_object = p_Tk_HolderObj
		else :
			if False :
				r_new_Tk_object = im_tkntr.Frame( p_Tk_HolderObj, side=im_tkntr.LEFT, bg=colour_bg_datatray())
				if True : # p_Obj4Gui.unplaced():
					dprint( "Packing Frame !!!!!!!!!!!!!!!!!" )
					r_new_Tk_object.pack( fill = im_tkntr.BOTH, expand=True )
				else:
					dprint( "Placing Frame -------" +str(p_Obj4Gui.place_x) +"," +str(p_Obj4Gui.place_y) +"," +str(p_Obj4Gui.place_w) +"," +str(p_Obj4Gui.place_h))
					r_new_Tk_object.place( x=p_Obj4Gui.place_x, y=p_Obj4Gui.place_y, width=p_Obj4Gui.place_w, height=p_Obj4Gui.place_h)
			else : # write ghost code for a bit
				# here we'd want to create:
				# - a left side frame, to hold buttons - navigation, edit etc side=LEFT
				# - a sideways tabbed Notebook, with two tabs:
				# - - a tab for the DataForm
				#     use Set_Obj_4Frm()
				# - - a tab for the DataSheet 
				#     use Set_Obj_4Sht()
				i_frame_trayctrls = im_tkntr.Frame( p_Tk_HolderObj, width=20, bg=colour_bg_neutral())
				i_frame_traytabs = im_tkntr.Frame( p_Tk_HolderObj, bg=colour_bg_datatray())
				i_frame_trayctrls.pack( side=im_tkntr.LEFT )
				i_frame_traytabs.pack( side=im_tkntr.RIGHT, fill = im_tkntr.BOTH, expand=True ) 
				# experimenting with setting a class item to the command=
				i_button_tray_load = im_tkntr.Button( i_frame_trayctrls, text="!", \
					command=ForTkLiveCallback( mdf_Tkinter_TrayFunction_RstLoad, p_dct_rtk, p_Obj4Data_DataBase, p_Obj4Gui) )
				i_button_tray_load.pack( side = im_tkntr.TOP, padx=1, pady=1) # im_tkntr.LEFT
				#
				i_button_tray_nav_1st = im_tkntr.Button( i_frame_trayctrls, text="[" ) # , command=treeview_nav_1st)
				i_button_tray_nav_1st.pack( side = im_tkntr.TOP, padx=1, pady=1) # im_tkntr.LEFT
				i_button_tray_nav_prv = im_tkntr.Button( i_frame_trayctrls, text="<" ) # , command=treeview_nav_prv)
				i_button_tray_nav_prv.pack( side = im_tkntr.TOP, padx=1, pady=1) # im_tkntr.LEFT
				i_button_tray_nav_nxt = im_tkntr.Button( i_frame_trayctrls, text=">" ) # , command=treeview_nav_nxt)
				i_button_tray_nav_nxt.pack( side = im_tkntr.TOP, padx=1, pady=1) # im_tkntr.LEFT
				i_button_tray_nav_lst = im_tkntr.Button( i_frame_trayctrls, text="]" ) # , command=treeview_nav_lst)
				i_button_tray_nav_lst.pack( side = im_tkntr.TOP, padx=1, pady=1) # im_tkntr.LEFT
				i_button_tray_nav_edt = im_tkntr.Button( i_frame_trayctrls, text="E" ) # , command=treeview_act_edit)
				i_button_tray_nav_edt.pack( side = im_tkntr.TOP, padx=1, pady=1) # im_tkntr.LEFT
				i_button_tray_nav_sav = im_tkntr.Button( i_frame_trayctrls, text="!" ) # , command=treeview_act_save)
				i_button_tray_nav_sav.pack( side = im_tkntr.TOP, padx=1, pady=1) # im_tkntr.LEFT
				#
				i_style = im_tkntr_ttk.Style( i_frame_traytabs)
				i_style.configure('lefttab.TNotebook', tabposition='ws')
				#
				notebook_tray = im_tkntr_ttk.Notebook( i_frame_traytabs, style='lefttab.TNotebook')
				tabframe_frm = im_tkntr.Frame( notebook_tray, bg=colour_bg_dataform(), width=800, height=200)
				tabframe_sht = im_tkntr.Frame( notebook_tray, bg=colour_bg_datasheet(), width=800, height=200)
				notebook_tray.add( tabframe_frm, text='Frm')
				notebook_tray.add( tabframe_sht, text='Sht')
				notebook_tray.pack()
				r_new_Tk_object = tabframe_frm
				# now store object references for the two frames: for the Form and the DataSheet
				rtk_For_ObjId_Set_TkObjRef( p_dct_rtk, p_Obj4Gui.id, rtk_Tktyp.rtk_Tktyp_Frame4Form, tabframe_frm )
				rtk_For_ObjId_Set_TkObjRef( p_dct_rtk, p_Obj4Gui.id, rtk_Tktyp.rtk_Tktyp_Frame4Sheet, tabframe_sht )
		r_newsub = True
	elif i_typ == Obj4GuiTyp.DataForm :
		dprint( "Now making a Frame for the DataForm" )
		r_new_Tk_object = im_tkntr.Frame( p_Tk_HolderObj, bg=colour_bg_dataform())
		if True : # p_Obj4Gui.unplaced():
			dprint( "Packing Frame !!!!!!!!!!!!!!!!!" )
			r_new_Tk_object.pack( fill = im_tkntr.BOTH, expand=True ) 
		else:
			dprint( "Placing Frame -------" +str(p_Obj4Gui.place_x) +"," +str(p_Obj4Gui.place_y) +"," +str(p_Obj4Gui.place_w) +"," +str(p_Obj4Gui.place_h))
			r_new_Tk_object.place( x=p_Obj4Gui.place_x, y=p_Obj4Gui.place_y, width=p_Obj4Gui.place_w, height=p_Obj4Gui.place_h)
		r_newsub = True
	elif i_typ == Obj4GuiTyp.DataSheet :
		dprint( "Making a DataSheet with a Treeview" )
		#input( "Press ENTER")
		# we need to get the rigjt object in which to place the TreeView
		i_Tk_Frame4Sheet = rtk_For_ObjId_Get_TkObjRef( p_dct_rtk, p_Prnt_Obj4Gui.id, rtk_Tktyp.rtk_Tktyp_Frame4Sheet )
		r_new_Tk_object = im_tkntr_ttk.Treeview( i_Tk_Frame4Sheet)
		#r_new_Tk_object.place( relheight=1.0, relwidth=1.0)  # fill the whole container with the treeview
		r_new_Tk_object.pack( fill="both", expand="yes" )  # fill the whole container with the treeview
		# adding scrollbars
		r_new_Tk_object_treescrolly = im_tkntr.Scrollbar( i_Tk_Frame4Sheet, orient="vertical", command=r_new_Tk_object.yview)
		r_new_Tk_object_treescrollx = im_tkntr.Scrollbar( i_Tk_Frame4Sheet, orient="horizontal", command=r_new_Tk_object.xview)
		r_new_Tk_object.configure( xscrollcommand=r_new_Tk_object_treescrollx.set, yscrollcommand=r_new_Tk_object_treescrolly.set)
		r_new_Tk_object_treescrollx.pack(side="bottom", fill="x")
		r_new_Tk_object_treescrolly.pack(side="right", fill="y")
		#r_new_Tk_object.bind("<<TreeviewSelect>>", t3_trvw_prnt_on_tree_select)
		## now put a reference for it in the run-time tkinter object dictionary
		rtk_For_ObjId_Set_TkObjRef( p_dct_rtk, p_Obj4Gui.id, rtk_Tktyp.rtk_Tktyp_TreeView, r_new_Tk_object ) # p_Dct_rtk, p_ObjId, p_TypId, p_TkObjRef
		r_newsub = True
	else :
		dprint( "Not Making Any Complex!!" )
	return r_newsub, r_new_Tk_object

def mdf_Tkinter_Window_Builder( p_TkinterBaseContainer, p_PyBaseObject, p_Obj4Data_DataBase):
	dprint( "mdf_Tkinter_Window_Builder")
	def mdf_Tkinter_Inner_Builder( p_Tk_HolderObj, p_Obj4Gui, p_Prnt_Obj4Gui):
		dprint( "mdf_Tkinter_Inner_Builder" + " id=" + str( p_Obj4Gui.id ) )
		i_typ = p_Obj4Gui.isTyp
		dprint( "p_Obj4Gui.isTyp: " + str(i_typ) + Obj4GuiTyp_Str( i_typ ) )
		if Obj4GuiTyp_Is_Complex( i_typ ) :
			i_didok, i_newTkObj = mdf_Tkinter_Build_Complex( i_dct_rtk, p_Tk_HolderObj, p_Obj4Data_DataBase, p_Obj4Gui, p_Prnt_Obj4Gui)
			if i_didok :
				dprint( "did mdf_Tkinter_Build_Complex ok")
				#input( "Press ENTER")
		else :
			i_didok, i_newTkObj = mdf_Tkinter_Build_Widget( p_Tk_HolderObj, p_Obj4Gui)
			if i_didok :
				dprint( "did mdf_Tkinter_Build_Widget ok")
				#input( "Press ENTER")
		if i_didok :
			dprint( "Considering deeper objects")
			try:
				i_subcount = len( p_Obj4Gui.contains )
			except:
				i_subcount = 0
			if i_subcount > 0 :
				dprint( "Looping contains")
				for i_sub_PyObj in p_Obj4Gui.contains :
					dprint( "Recursing into what p_Obj4Gui contains")
					mdf_Tkinter_Inner_Builder( i_newTkObj, i_sub_PyObj, p_Obj4Gui)
			else :
				dprint( "Saw nothing in Contains")
		else :
			dprint( "did NOT build this time.")
	# now assumes we're passed a generic container object i.e. not interested in its own details
	if p_PyBaseObject.isTyp == Obj4GuiTyp.Window :
		dprint( "mdf_Tkinter_Window_Builder" + " processing a Window's contents")
		for i_sub0_PyObj in p_PyBaseObject.contains :
			dprint( "mdf_Tkinter_Window_Builder" + " content id=" + str( i_sub0_PyObj.id ) )
			i_dct_rtk = {}
			mdf_Tkinter_Inner_Builder( p_TkinterBaseContainer, i_sub0_PyObj, p_PyBaseObject)
	else :
		dprint( "Oops! No Window, Got IsTyp:" +  str(p_PyBaseObject.isTyp))

# --------------------------------------------------

# --------------------------------------------------
# Test calls
# --------------------------------------------------


def gui_Build_Test( p_PyWindowOb4Tk, p_Obj4Data_DataBase ) :
	dprint("gui_Build_Test")
	root_window = im_tkntr.Tk()
	#
	tk_fonts = [ 'TkDefaultFont', 'TkMenuFont', 'TkHeadingFont', 'TkTooltipFont', 'TkTextFont', 'TkCaptionFont', 'TkSmallCaptionFont', 'TkFixedFont', 'TkIconFont' ]
	font_defs = {}
	new_fnt_size = 10
	for tkf in tk_fonts :
		font_defs[ tkf ] = m_tkntr_font.nametofont(tkf)
		font_defs[ tkf ].config( size = new_fnt_size )
	#
	root_window.title(app_show_name())
	root_window.wm_geometry("780x500")
	# define the main window with tkinter features
	mdf_Tkinter_Window_Builder( root_window, p_PyWindowOb4Tk, p_Obj4Data_DataBase )
	# get the GUI in action
	root_window.mainloop()

def Load_XML_and_Build_GUI_Test( ) :
	dprint("Load_XML_and_Build_GUI_Test")
	# Ingest the XML
	i_ok, i_Meta_Obj4Gui, i_Obj4Data_DataBase = mdf_Process_XML_File( ExampleDetail_MetaDataForm_Filename() )
	# Build the Tkinter object
	if i_ok :
		i_dct_Obj4Gui = i_Meta_Obj4Gui.DctWindows
		if len( i_dct_Obj4Gui ) > 0 :
			lst_keys = list(i_dct_Obj4Gui.keys())
			gui_Build_Test( i_dct_Obj4Gui[ lst_keys[0] ], i_Obj4Data_DataBase )
		else :
			dprint("No windows.")
	else :
		dprint("Failed: mdf_Process_XML_File")

# --------------------------------------------------
# GUI Tkinter Code building
# Use the assembled TkObj tree to build stock code for Tkinter objects 
# --------------------------------------------------

def mdf_Tkinter_LinesOfCodeBuild_Add_Def( p_Lst_Lines, p_new_def_name ):
	# make lines of code that can be added to the codebase as a framework into which actual code can be later written
	s = " "
	p_Lst_Lines.append( s)
	s = "def " + new_def_name + "():"
	p_Lst_Lines.append( s)
	s = "\t" + "pass"
	p_Lst_Lines.append( s)

def gui_GeneratePythonCode( p_dct_PyObjStructure ) :
	pass

# --------------------------------------------------
# Interactive - Support
# --------------------------------------------------

def getpathref_fromuser( show_title, from_folder_name) :
	# Brief:
	# present a dialog box for selection of a folder path
	# Parameters:
	# - show_title - string to put on the title bar
	# - from_folder_name - location from which to initially show the folder tree
	# Returns:
	# - a string of the selected folder, or an empty string for no selection
	# Notes:
	# Code:
	if len(show_title) == 0 :
		show_title = "Select a Folder"
	if len(from_folder_name) == 0 :
		from_folder_name = str( im_pathlib.Path.home() )
	root = im_tkntr.Tk()
	root.withdraw()
	i_foldref = im_filedialog.askdirectory( title=show_title, initialdir=from_folder_name)
	return i_foldref

def getloadfileref_fromuser( show_title, from_folder_name) :
	# Brief:
	# present a dialog box for selection of a folder path
	# Parameters:
	# - show_title - string to put on the title bar
	# - from_folder_name - location from which to initially show the folder tree
	# Returns:
	# - a string of the selected folder, or an empty string for no selection
	# Notes:
	# Code:
	if len(show_title) == 0 :
		show_title = "Select a File to Load from"
	if len(from_folder_name) == 0 :
		from_folder_name = str( im_pathlib.Path.home() )
	root = im_tkntr.Tk()
	root.withdraw()
	i_fileref = im_filedialog.askopenfilename( title=show_title, initialdir=from_folder_name)
	return i_fileref

def getsavefileref_fromuser( show_title, from_folder_name) :
	# Brief:
	# present a dialog box for selection of a folder path
	# Parameters:
	# - show_title - string to put on the title bar
	# - from_folder_name - location from which to initially show the folder tree
	# Returns:
	# - a string of the selected folder, or an empty string for no selection
	# Notes:
	# Code:
	if len(show_title) == 0 :
		show_title = "Select a File to Save to"
	if len(from_folder_name) == 0 :
		from_folder_name = str( im_pathlib.Path.home() )
	root = im_tkntr.Tk()
	root.withdraw()
	i_fileref = im_filedialog.asksaveasfilename( title=show_title, initialdir=from_folder_name)
	return i_fileref


def From_DctWindows_PullFor_ComboBox( p_Dct_Windows ): # gets passed i_Meta_Obj4Gui.DctWindows
	r_dct_4_combo = {} # dictionary to feed combobox, keys are the display strings for user to see
	for k in p_Dct_Windows.keys():
		i_obj = p_Dct_Windows[k] # this should be a window object, class: Obj4Gui_Window
		r_dct_4_combo[ i_obj.name ] = k # the key to the window object
	return r_dct_4_combo


# --------------------------------------------------
# Interactive - Built from XML
# --------------------------------------------------

def mdf_Tkinter_LiveMakeSelection( ):
	# This is an interaction window allowing the live user to select an XML file (i.e. MDFML) to load
	# it will then populate the listbox with the available Forms
	# the user can then select one and execute it
	i_Meta_Obj4Gui = Obj4Gui_Meta() # meaningless really, as it will be overwritten on load
	i_Obj4Data_DataBase = Obj4Data_DataBase( Obj4Data_DataBaseTyp.Sqlite ) # will be overwritten on load
	i_dct_4_combo = {}
	def Cmd_button_exit() :
		dprint( "Live: Quitting") 
		root_window.quit()
	def Cmd_button_sdb_file_browse() :
		nonlocal i_Obj4Data_DataBase
		dprint( "Live: Browsing") 
		got_path = tk_var_sdb_file.get()
		got_string = getloadfileref_fromuser( "Select SQLite database File", got_path)
		if len( got_string ) > 0 :
			tk_var_sdb_file.set( got_string)
	def Cmd_button_sdb_file_load() :
		nonlocal i_Obj4Data_DataBase
		i_fn = tk_var_sdb_file.get()
		i_Obj4Data_DataBase.set_db_filename( i_fn )
		got_fn  = i_Obj4Data_DataBase.get_db_filename()
		tk_label_sdb_file.configure( text = "Set: " + got_fn)
	def Cmd_button_xml_file_browse() :
		dprint( "Live: Browsing") 
		got_path = tk_var_xml_file.get()
		got_string = getloadfileref_fromuser( "Select MDFML (XML) File", got_path)
		if len( got_string ) > 0 :
			tk_var_xml_file.set( got_string) # ExampleDetail_MetaDataForm_Filename()
	def Cmd_button_xml_file_load() :
		dprint( "Live: Loading") 
		nonlocal i_Meta_Obj4Gui
		nonlocal i_Obj4Data_DataBase
		nonlocal i_dct_4_combo
		got_filename = tk_var_xml_file.get()
		if len( got_filename ) > 0 :
			i_ok, i_Meta_Obj4Gui, i_Obj4Data_DataBase = mdf_Process_XML_File( got_filename )
		dprint( "Live: loaded " + str(len(i_Meta_Obj4Gui.DctWindows)) )
		i_dct_4_combo = From_DctWindows_PullFor_ComboBox( i_Meta_Obj4Gui.DctWindows )
		for i_key in sorted( i_dct_4_combo.keys()) :
			dprint( "Live: listing: key " + str( i_key ) + " " + str(i_dct_4_combo[i_key]) )
			tk_lbx_wndwlist.insert( 0, i_key )
	def Cmd_button_open() :
		nonlocal i_Obj4Data_DataBase
		dprint( "Live: Opening") 
		i_selection = 33
		lst_index = tk_lbx_wndwlist.curselection()
		if len( lst_index) > 0 :
			i_index = int(lst_index[0])
			i_selection = tk_lbx_wndwlist.get(i_index)
		if i_selection in i_dct_4_combo:
			i_wk = i_dct_4_combo[ i_selection ]
			if i_wk in i_Meta_Obj4Gui.DctWindows :
				dprint( "Live: we have something to Open") 
				# if not acquired from the XML load then apply database filename from GUI
				if len( i_Obj4Data_DataBase.get_db_filename() ) == 0 :
					Cmd_button_sdb_file_load()
				# get that Obj4Gui definition set
				i_Obj4Gui = i_Meta_Obj4Gui.DctWindows[ i_wk ]
				tk_window_new = im_tkntr.Toplevel( root_window )
				tk_window_new.title("Test form " + str(i_wk) + " " + i_selection)
				tk_window_new.wm_geometry("1000x800")
				dprint( "Will build a Window passing database reference: " + i_Obj4Data_DataBase.get_db_filename() )
				mdf_Tkinter_Window_Builder( tk_window_new, i_Obj4Gui, i_Obj4Data_DataBase)
	# --------------
	dprint("mdf_Tkinter_LiveMakeSelection")
	# make a Window which will list the available Forms to demonstrate
	root_window = im_tkntr.Tk()
	tk_fonts = [ 'TkDefaultFont', 'TkMenuFont', 'TkHeadingFont', 'TkTooltipFont', 'TkTextFont', 'TkCaptionFont', 'TkSmallCaptionFont', 'TkFixedFont', 'TkIconFont' ]
	font_defs = {}
	new_fnt_size = 8
	for tkf in tk_fonts :
		font_defs[ tkf ] = m_tkntr_font.nametofont(tkf)
		font_defs[ tkf ].config( size = new_fnt_size )
	root_window.title(app_show_name())
	root_window.wm_geometry("780x500")
	# --------------
	# make top frame
	tk_frame_top = im_tkntr.Frame( root_window)
	tk_frame_top.pack( fill = im_tkntr.X)
	tk_button_exit = im_tkntr.Button( tk_frame_top, text="(X)", command=Cmd_button_exit)
	tk_label_title = im_tkntr.Label( tk_frame_top, text=app_show_name_version() )
	tk_label_explain = im_tkntr.Label( tk_frame_top, text=app_show_subtitle())
	# place the content
	tk_button_exit.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	tk_label_title.pack( side = im_tkntr.LEFT, padx=5, pady=5)
	tk_label_explain.pack( side = im_tkntr.RIGHT, padx=5, pady=5)
	# --------------
	# frame for the SQLite database file selection
	tk_frame_sdb_file = im_tkntr.Frame( root_window, bg=colour_bg_safely())
	tk_frame_sdb_file.pack( fill = im_tkntr.X)
	tk_label_browse_sdb_file = im_tkntr.Label( tk_frame_sdb_file, text="SQLite database file:", bg=colour_bg_safely())
	tk_var_sdb_file = im_tkntr.StringVar()
	tk_entry_sdb_file = im_tkntr.Entry( tk_frame_sdb_file, textvariable=tk_var_sdb_file)
	tk_button_browse_sdb_file = im_tkntr.Button( tk_frame_sdb_file, text="Browse..", command=Cmd_button_sdb_file_browse)
	tk_button_load_sdb_file = im_tkntr.Button( tk_frame_sdb_file, text="Set", command=Cmd_button_sdb_file_load)
	#  layout
	tk_label_browse_sdb_file.pack( side = im_tkntr.LEFT, padx=5, pady=5) 
	tk_entry_sdb_file.pack( side = im_tkntr.LEFT, fill = im_tkntr.X, expand=1, padx=5, pady=5) 
	tk_button_browse_sdb_file.pack( side = im_tkntr.LEFT, padx=5, pady=5)
	tk_button_load_sdb_file.pack( side = im_tkntr.LEFT, padx=5, pady=5)
	# --------------
	# frame for displaying the current database object settings
	tk_frame_sdb_sets = im_tkntr.Frame( root_window, bg=colour_bg_neutral())
	tk_frame_sdb_sets.pack( fill = im_tkntr.X)
	tk_label_sdb_file = im_tkntr.Label( tk_frame_sdb_sets, text="No filename currently set", bg=colour_bg_neutral())
	tk_label_sdb_file.pack( side = im_tkntr.LEFT, fill = im_tkntr.X, padx=5, pady=5) 
	#
	# --------------
	# frame for the XML file selection
	tk_frame_xml_file = im_tkntr.Frame( root_window, bg=colour_bg_safely())
	tk_frame_xml_file.pack( fill = im_tkntr.X)
	tk_label_browse_xml_file = im_tkntr.Label( tk_frame_xml_file, text="XML file:", bg=colour_bg_safely())
	tk_var_xml_file = im_tkntr.StringVar()
	tk_entry_xml_file = im_tkntr.Entry( tk_frame_xml_file, textvariable=tk_var_xml_file)
	tk_button_browse_xml_file = im_tkntr.Button( tk_frame_xml_file, text="Browse..", command=Cmd_button_xml_file_browse)
	tk_button_load_xml_file = im_tkntr.Button( tk_frame_xml_file, text="Load", command=Cmd_button_xml_file_load)
	#  layout
	tk_label_browse_xml_file.pack( side = im_tkntr.LEFT, padx=5, pady=5) 
	tk_entry_xml_file.pack( side = im_tkntr.LEFT, fill = im_tkntr.X, expand=True, padx=5, pady=5) 
	tk_button_browse_xml_file.pack( side = im_tkntr.LEFT, padx=5, pady=5)
	tk_button_load_xml_file.pack( side = im_tkntr.LEFT, padx=5, pady=5)
	#
	tk_var_xml_file.set( ExampleDetail_MetaDataForm_Filename() )
	# --------------
	# frame for the Form selection
	tk_frame_forms = im_tkntr.Frame( root_window, bg=colour_bg_safely())
	tk_frame_forms.pack( fill = im_tkntr.BOTH, expand=True)
	# make a selection list of the 
	tk_lbx_wndwlist = im_tkntr.Listbox( tk_frame_forms ) # , height=2 )
	tk_lbx_wndwlist.pack( side = im_tkntr.LEFT, fill=im_tkntr.BOTH, expand=True)
	# tk_lbx_wndwlist.bind('<Double-Button>',  lambda x: t1_on_dblclck_selected_disposal() )
	tk_button_goto = im_tkntr.Button( tk_frame_forms, text="Open", command=Cmd_button_open)
	tk_button_goto.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	# get the GUI into action
	root_window.mainloop()

# --------------------------------------------------
# Main
# --------------------------------------------------

# main to execute only if run as a program
if __name__ == "__main__":
	if __debug__:
		pass
	dclear()
	if True :
		mdf_Tkinter_LiveMakeSelection( )
	elif True :
		Load_XML_and_Build_GUI_Test()
